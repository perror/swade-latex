\documentclass[a4paper]{swade-article}

\usepackage{minted}
\usepackage[subtle]{savetrees}

\title{SWADE\;\LaTeX\ Documentation}
\subtitle{How-to use this \LaTeX\ template\dots}

\author{Emmanuel Fleury}

\coverpicture{pictures/example-pict.jpg}

\version{0.7.2}

\begin{document}

\maketitle

\begin{abstract}
 The SWADE \LaTeX\ template is a set of \LaTeX\ classes designed to
 help to produce high typographic quality documents with a minimum of
 effort for the Savage Worlds Adventure Edition by Pinnacle
 Entertainment Group. This is entirely devoted for fan products.
\end{abstract}

\tableofcontents

\newpage

\section{Classes and options}

This \LaTeX\ package comes with the following classes:
\begin{itemize}
\item \texttt{swade-cards}: Create your own adventure cards;
\item \texttt{swade-onesheet}: For onesheet documents which should not
  exceed about 10 pages. Typically, a quick adventure, a quick
  conversion guide to SWADE;
\item \texttt{swade-article}: For medium ranged documents which should
  not exceed about 25-30 pages. Typically this correspond to optional
  rules proposals, one-shot or short adventures or a small setting;
\item \texttt{swade-book}: For long ranged documents which are more
  than 25-30 pages. Typically, this may be a campaign or a full setting.
\end{itemize}

Basically, you can use any of these classes as follow:

\begin{minted}[frame=single]{latex}
 \documentclass[op1,op2]{swade-article}
\end{minted}

Except for \texttt{swade-cards} which is a special class (see
section~\ref{sect:cards} p.~\pageref{sect:cards}), the options that
you can find in all the classes are the following:

\begin{itemize}
\item \textbf{Paper format}: \texttt{a4paper}, \texttt{a5paper},
  \texttt{letterpaper};

\item \textbf{Font size}: \texttt{8pt}, \texttt{9pt}, \texttt{10pt},
  \texttt{11pt}, \texttt{12pt}.

\item \textbf{Orientation}: \texttt{landscape} (portrait by default);

\item \textbf{Title page} (\texttt{swade-article} only):
  \texttt{titlepage};

\item \textbf{Printer friendly}: \texttt{print} (Remove unnecessary
  background images and make it more B\&W).
\end{itemize}

\section{Localization}

You may switch the template to another language by using the
\texttt{babel} package. For example:

\begin{minted}[frame=single]{latex}
 \usepackage[french]{babel}
\end{minted}

\section{Document structure}

The first usage of \LaTeX\ is to help to structure your document. It
must have a front matter with a title page and references to the author(s)
and, possibly, a table of contents. Then, comes the main matter of the
document organized in parts, chapters, sections, \dots Finally, a back
matter with all the appendix sections.

\subsection{Title and authoring}

Before the \mintinline{latex}{\begin{document}}, you can set several
  parameters about the document (title, subtitle, \dots):

\begin{minted}[frame=single]{latex}
\title{Documentation title}
\subtitle{My subtitle punchline
\author{Author Name}
\date{\today}
\version{1.0}
\end{minted}

Then, creating a front page with the title is achieved when you
write after the \mintinline{latex}{\begin{document}}:

\begin{minted}[frame=single]{latex}
 \maketitle
\end{minted}

  You can also redefine the copyright notice (after
  \mintinline{latex}{\begin{document}}) with, for example:

\begin{minted}[frame=single]{latex}
\copyrightnotice{%
  \begin{tikzpicture}[remember picture,
                      overlay]
   \matrix[matrix of nodes,
           nodes={inner sep=0,
                  outer sep=0,
                  align=center},
           ampersand replacement=\&,
           anchor=center,
           inner sep=0,outer sep=0,
           minimum width=.3\paperwidth]
      at (current page.south)
      {
        AAAA \& BBBB \& CCCC \\
      };
  \end{tikzpicture}
}
\end{minted}




If the \texttt{titlepage} option has been called, you can set a
picture to appear on the titlepage:

\begin{minted}[frame=single]{latex}
\coverpicture{example.jpg}
\end{minted}

Or, you may just redefine the '\texttt{titlepage}' command with a
'\texttt{renewcommand}' command.

\subsection{Table of contents}

The table of contents of the document can be displayed if you add the
following line in the document:

\begin{minted}[frame=single]{latex}
\tableofcontents
\end{minted}

\subsection{Sections hierarchy}

The document may be structured with the following section types
(ordered from the highest to the lowest scope). You may ask for a
different name in the table of contents by passing an option to the
command (as for the \texttt{\textbackslash{}part} command in the
example).

\begin{minted}[frame=single]{latex}
\part[toc name]{Part name}
\chapter{Chapter name}
\section{Section name}
\subsection{Subsection name}
\subsubsection{Subsubsection name}
\paragraph{Paragraph name}
\end{minted}

Note that the \texttt{swade-book} class has all the hierarchy, but
\texttt{swade-article} and \texttt{swade-onesheet} only start at the
\texttt{\textbackslash{}section} level of the hierarchy.

\subsection{Making an appendix}

You may want to add additional sections or chapters that are not
essential for the understanding of the document. Marking the start of
the appendix is simply like this:

\begin{minted}[frame=single]{latex}
\appendix
\section{First section of my appendix}
\end{minted}

\subsection{Changing columns number}

You may switch from the two columns setting to a full page by using
the \mintinline{latex}{\onecolumn} command and get back to two columns
with the \mintinline{latex}{\twocolumn} command.

\section{Typographical emphasis}

Here are a few styles in which you can write:

\begin{minted}[frame=single]{latex}
\textrm{Roman}
\textbf{Boldface}
\textit{Italic}
\texttt{Typewriter}
\textsc{Small Caps}
\textsf{Sans Serif}
\textsl{Slanted}
\end{minted}

Note also that there is a way to stress the emphasis of some words
depending on the context (if in an italic context, the result will be
roman and reverse):

\begin{minted}[frame=single]{latex}
\emph{Emphasis}
\end{minted}

One can also change the size of the text as follow:

\begin{minted}[frame=single]{latex}
{\tiny Tiny size}
{\scriptsize Script size}
{\footnotesize Footnote size}
{\small Small size}
{\normalsize Normal size}
{\large Large size}
{\Large Larger size}
{\LARGE Even larger size}
{\huge Huge size}
{\Huge Huger size}
{\HUGE Even huger size}
\end{minted}

\subsection{Footnotes}

The footnotes are easily done as follow:

\begin{minted}[frame=single]{latex}
Some text\footnote{My footnote text.}
\end{minted}

And, they will be displayed as follow:

\begin{framed}
  Some text\footnotemark
\end{framed}
\footnotetext{My footnote text.}

\begin{savagetable*}{LCR}{Table Title}
  \headrow One & Two & Three \\
  Men & Mice & Lions \\
  Upper & Middle & Lower \\
\end{savagetable*}

\section{List of items}

You may need to enumerate items in a list and require to display it as
an itemized list. Here are some ways to do it.

\subsection{Non-ordered lists}

The following example:

\begin{minted}[frame=single]{latex}
\begin{itemize}
\item An item in the list;
\item Another item in the list with a
 long text that goes to the next line;
\item An item with a sublist:
  \begin{itemize}
  \item A subitem with a subsublist:
    \begin{itemize}
    \item Last item in the list.
    \end{itemize}
  \end{itemize}
\end{itemize}
\end{minted}

Will appear as follows:

\begin{framed}
  \begin{itemize}
  \item An item in the list;
  \item Another item in the list with a long text that goes to the
    next line;
  \item An item with a sublist:
    \begin{itemize}
    \item A sub-item with a subsublist:
      \begin{itemize}
      \item Last item in the list.
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{framed}

Also, when listing the characters of a scene for a scenario, one can
use the item \mintinline{latex}{\jokeritem}:

\begin{minted}[frame=single]{latex}
\begin{itemize}
  \item \textbf{\textsc{Spearmen}\,(5):}
    Take profile of Soldier p.~48;
  \item \textbf{\textsc{Archers}\,(1 per
    hero)};
  \jokeritem \textbf{\textsc{Sheriff of
    Nottingham}}.
\end{itemize}
\end{minted}

Which will display like that:

\begin{framed}
\begin{itemize}
  \item \textbf{\textsc{Spearmen}\,(5):}
    Take profile of Soldier p.~48;
  \item \textbf{\textsc{Archers}\,(1 per hero)};
  \jokeritem \textbf{\textsc{Sheriff of Nottingham}}.
\end{itemize}
\end{framed}


\subsection{Ordered lists}

The following example:

\begin{minted}[frame=single]{latex}
\begin{enumerare}
\item An item in the list;
\item Another item in the list with a
 long text that goes to the next line;
\item An item with a sublist:
  \begin{enumerate}
  \item A subitem with a subsublist:
    \begin{enumerate}
    \item Last item in the list.
    \end{enumerate}
  \end{enumerate}
\end{enumerate}
\end{minted}

Will appear as follows:
\begin{framed}
  \begin{enumerate}
  \item An item in the list;
  \item Another item in the list with a long text that goes to the
    other line;
  \item An item with a sublist:
    \begin{enumerate}
    \item A subitem with a subsublist:
      \begin{enumerate}
      \item Last item in the list.
      \end{enumerate}
    \end{enumerate}
  \end{enumerate}
\end{framed}


\section{Tables}

Data are often better to understand when represented as a table, here
is the \LaTeX\ way to write a table followed by the final rendering of
it in the document. The letters '\texttt{L}' (span and left-align),
'\texttt{C}' (span and center align), '\texttt{R}' (span and
right-align), '\texttt{P}' (center align and do not span) are defining
text alignment inside the column cell. There is only one special
command: \texttt{\textbackslash{}headrow} which sets the current row
as a header.

\begin{minted}[frame=single]{latex}
\begin{savagetable}{LCR}{Table Title}
  \headrow One & Two & Three \\
  Men & Mice & Lions \\
  Upper & Middle & Lower \\
\end{savagetable}
\end{minted}

\begin{framed}
\begin{savagetable}{LCR}{Table Title}
  \headrow One & Two & Three \\
  Men & Mice & Lions \\
  Upper & Middle & Lower \\
\end{savagetable}
\vspace{-1em}
\end{framed}

Note that if you omit the title of the table, it takes a simpler form.

\begin{minted}[frame=single]{latex}
\begin{savagetable}{LCR}{}
  \headrow One & Two & Three \\
  Men & Mice & Lions \\
  Upper & Middle & Lower \\
\end{savagetable}
\end{minted}

\begin{framed}
\begin{savagetable}{LCR}{}
  \headrow One & Two & Three \\
  Men & Mice & Lions \\
  Upper & Middle & Lower \\
\end{savagetable}
\end{framed}

Finally, you can create a table all across the page by using the
starred environment \texttt{savagetable*} (see the table at the top of
the page as an example).

Note that the starred environment has an optional argument
'\texttt{t}' (top), '\texttt{b}' (bottom), '\texttt{m}' (middle) set
the position of the image on the page. But, as a float, it will appear
only on the next page (as the previous column may have been already
set and cannot be undone).

\savagepicture*{2018~@~Pinnacle Entertainment Group}{pictures/example-pict.jpg}

\section{Pictures}

If you want to display a picture in the text, you can use the
following commands (the author field can be left empty):

\begin{minted}[frame=single]{latex}
\savagepicture[options]{author}{pict.jpg}
\end{minted}

The \texttt{\textbackslash{}savagepicture\{\}\{\}} command displays the
image within a column (see below) and the starred command
(\texttt{\textbackslash{}savagepicture*\{\}}) displays the image
across the two columns on the top of the page (see above). Beware, the
command must be placed on the page before it should appear (the page
layout is computed at the beginning of the page, not at the end). The
\texttt{options} are the ones from the
\texttt{\textbackslash{}includegraphics\{\}} command. Feel free to use
any of these options with \texttt{\textbackslash{}savagepicture\{\}\{\}}.

\savagepicture{2018~@~Pinnacle Entertainment Group}{pictures/example-pict.jpg}


\section{Environments}

\subsection{The \texttt{quote} environment}

Sometimes, you may need to write a text destined to be read to the
players (for example during an adventure). This is achieved by the
\texttt{quote} environment:

\begin{minted}[frame=single]{latex}
\begin{quote}
  This text will appear as quoted in
  a different environment in order not
  to be confused with the normal text
  of the document.
\end{quote}
\end{minted}

And, this will be displayed as follow:

\begin{framed}
  \begin{quote}
    This text will appear as quoted in a different environment in
    order not to be confused with the normal text of the document.
  \end{quote}
\end{framed}

\subsection{Note environment}

The '\texttt{note}' environment can be used to write side notes. By
default it has several possible backgrounds (\texttt{0}--\texttt{5})
which can chosen as an option to the command. Note also that the
'\texttt{note*}' environment span along the full page width.

\begin{minted}[frame=single]{latex}
\begin{note}[<0-5>]{<title>}
  <text>
\end{note}
\end{minted}

It displays as follows:

\begin{framed}
  \begin{note}{A Note Example}
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ut
    massa odio. Nullam finibus ac risus vitae molestie. Proin aliquam
    quam ac massa vulputate efficitur non in eros.
  \end{note}
\end{framed}


\subsection{Statblocks}

It is always useful to show the statistics of a character or a
monster, the \texttt{statblock} environment is especially designed for
that. Extras use the \texttt{statblock} environment and Jokers will
use the \texttt{statblock*} environment. The statblock command usage
is as follow:

\begin{minted}[frame=single]{latex}
\begin{statblock}[<wounds>]{<title>}{%
    <setting statistics>
  }
  \block{<title>}
  <text>
\end{statblock}
\end{minted}

And here is a full documented example:

\begin{minted}[frame=single]{latex}
\begin{statblock*}[(1)2]{Herodes}{
  %%%% Settings statistics %%%%
  % Attributes
  \agility{d6}
  \smarts{d8}
  \spirit{d6}
  \strength{d4}
  \vigor{d6}
  % Derived statistics
  \pace{6, Aquatic 10}
  \parry{4}
  \toughness{8\,(3)}
  % Skills
  \skills{Athletics d4, Common
    Knowlegde d6, Fight d4, Notice d6,
    Persuasion d6, Shooting d4,
    Stealth d6.}
  % Edges and hindrances
  \edges{Command, Rich.}
  \hindrances{Greedy (Major), Yellow.}
  %%%% Setting statistics end %%%%
  }

  % Starting custom blocks
  \block{Special abilities}
  \begin{itemize}
  \ability{Command} +1 to Extras' Shaken
  or Stunned recovery rolls in Command
  Range (5 squares).
  \ability*{Armor +3} Bronze breastplate.
  \end{itemize}

  \block{Actions}
  \begin{itemize}
  \fight{Gladius} Fight d4, 2d4.
    \begin{itemize}
    \modifier{Frenzy} Roll a second
    Fighting die with one melee attack
    per turn.
    \end{itemize}
  \shoot{Short bow} Shooting d4-1,
    12/24/48, 2d6.
  \end{itemize}

  \block{Gears}
  Bronze breastplate (+3), bronze
  greaves (+2), legionary helmet (+3),
  gladius, short bow, horse.
\end{statblock*}
\end{minted}

Note that there are several specific items shapes:
\begin{itemize}
\item \mintinline{latex}{\fight{}}: Refer to a Figh skill action;
\item \mintinline{latex}{\shoot{}}: Refer to a Shoot skill action;
\item \mintinline{latex}{\ability{}}: Refer to an Ability;
\item \mintinline{latex}{\ability*{}}: Refer to an Ability that
  includes its technicals in its label (\emph{eg} Armor, Size, \dots);
\item \mintinline{latex}{\power{}}: Refer to an Arcana Power;
\item \mintinline{latex}{\modifier{}}: Refer to an Ability modifying
  an action (\emph{eg} \textbf{Rock'n'Roll!} on machine gun, \dots);
\item \mintinline{latex}{\other{}}: Refer to any other items.
\end{itemize}

Optionally, you can change the wounds of the character in the options
of the '\texttt{statblock}' environment by giving the number of wounds
and you can also specify which wounds will not cause a malus by using
parenthesis. For example, if you want to ignore the first wound for a
malus then write '\texttt{(1)2}'. Beware, you cannot nest the
parenthesis, you can only have one level of parenthesis.

More commands to lists Edges or Hindrances exists, you can use
\mintinline{latex}{\languages{langs}} to list the languages mastered
by the character and you can also use the
\mintinline{latex}{\cyberware{strain}{cybers}} to display the list of
cyberware worn to the character. And, you can also set a specific
attribute for your setting with \mintinline{latex}{\settingattr{}} and
\mintinline{latex}{\settingattrname{}} to give it a name to it.

Finally, the statblock example displays as follow:

\begin{statblock*}[(1)2]{Herodes}{
  % Settings statistics
  % Attributes
  \agility{d6}
  \smarts{d8}
  \spirit{d6}
  \strength{d4}
  \vigor{d6}

  % Derived statistics
  \pace{6, Aquatic 10}
  \parry{4}
  \toughness{8(3)}

  % Skills
  \skills{Athletics d4, Common
    Knowlegde d6, Fight~d4, Notice d6,
    Persuasion d6, Shooting d4,
    Stealth d6.}

  % Edges and hindrances
  \edges{Command, Rich.}
  \hindrances{Greedy (Major), Yellow.}
  }

  % Starting custom blocks
  \block{Special abilities}
  \begin{itemize}
  \ability{Command} +1 to Extras' Shaken or
  Stunned recovery rolls in Command
  Range (5 squares).
  \ability*{Armor +3} Bronze breastplate.
  \end{itemize}

  \block{Actions}
  \begin{itemize}
  \fight{Gladius} Fight d4, 2d4.
    \begin{itemize}
    \modifier{Frenzy} Roll a second Fighting
    die with one melee attack per turn.
    \end{itemize}
  \shoot{Short bow} Shooting d4-1,
    12/24/48, 2d6.
  \end{itemize}

  \block{Gears}
  Bronze breastplate (+3), bronze
  greaves (+2), legionary helmet (+3),
  gladius, short bow, horse.
\end{statblock*}


\section{Cross-references}

You can refer to other part of the document and get it page number by
labeling either text or hook on any section, subsection and so on.
Here is a code example on how to use it. Note that the
'\texttt{\textasciitilde{}}' is just a non-breakable space in \LaTeX.

\begin{minted}[frame=single]{latex}
\section{Section name}
 \label{sec:secname}
  ...
  My example text refer to section
  (see~p.\pageref{sec:secname}).
\end{minted}

Note that, as the style does not display the section numbers, the
\texttt{\textbackslash{}pageref\{\}} is the only recommended way to
refer to other parts of the document.


\section{Adventure cards}
\label{sect:cards}

Basically, you can use any of these classes as follow:

\begin{minted}[frame=single]{latex}
\documentclass[op1,op2]{swade-cards}
\end{minted}

The options that you can find in this classes are the following:

\begin{itemize}
\item \textbf{Paper format}: \texttt{a4paper}, \texttt{letterpaper};

\item \textbf{Card format}: \texttt{oversized} (B7, 88$\times$125mm)\\
  (normal (B8, 62$\times$88~mm) by default);
\end{itemize}


\begin{minted}[frame=single]{latex}
\card{pic.jpg}{Team Work}{%
   All Support bonus are doubled for
   the round on all allied characters.
}
\end{minted}

\begin{center}
  \includegraphics[scale=.85]{pictures/adventure-card-example.jpg}
\end{center}
\medskip

\section{Credits}
\begin{itemize}
\item \textbf{Author}: \theauthor
\item \textbf{Date}: \today
\item \textbf{Version}: \theversion
\end{itemize}

\end{document}
