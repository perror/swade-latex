%%
%% This file provide a LaTeX document class for articles written by
%% fans of the Savage Worlds RPG. This is an open-source work, so feel
%% free to share it and to modify it according to the license below.
%%
%% -------------------------------------------------------------------
%%
%% Copyright (C) 2021, Emmanuel Fleury <emmanuel.fleury@gmail.com>
%%
%% Permission to use, copy, modify, and/or distribute this software
%% for any purpose with or without fee is hereby granted.
%%
%% THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
%% WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
%% AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
%% CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
%% LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
%% NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
%% CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
%%

\NeedsTeXFormat{LaTeX2e}[2005/12/01]
\ProvidesPackage{torgan}[2021 1.0 Torgan SWADE style package]

%% Specific Settings
\newif\if@deadlands   \@deadlandsfalse  %% Deadlands Reloaded
\newif\if@etu         \@etufalse        %% East Texas University
\newif\if@lankhmar    \@lankhmarfalse   %% Lankhmar
\newif\if@lastparsec  \@lastparsecfalse %% The Last Parsec

% Class options
\DeclareOption{etu}{\@etutrue}%
\DeclareOption{deadlands}{\@deadlandstrue}%
\DeclareOption{lastparsec}{\@lastparsectrue}%
%
\ProcessOptions\relax


% Set Glyphs
\AtBeginDocument{%
\if@deadlands
  % Specialize Glyphs for Western !
  \renewcommand\shootglyph{%
    \raisebox{-.25em}[0pt][0pt]{%
      \includegraphics[height=.95\baselineskip]{glyphs/westerngun.png}\!\!\!\!
    }
  }
  \def\thesettingname{Deadlands\string:~The Weird West}
\fi

\if@lastparsec
  % Specialize for scifi !
  \renewcommand\shootglyph{%
    \raisebox{-.1em}[0pt][0pt]{%
      \includegraphics[height=.75\baselineskip]{glyphs/lasergun2.png}\!\!\!\!
    }
  }
  \def\thesettingname{The~Last~Parsec}
\fi

\if@etu
  % Specialize for modern !
  \renewcommand\shootglyph{%
    \raisebox{-.1em}[0pt][0pt]{%
      \includegraphics[height=\baselineskip]{glyphs/gun.png}\!\!\!\!
    }
  }
  \def\thesettingname{East~Texas~University}
  \settingattrname{Scolarité}
\fi
}

%% Default Torgan stamp
\def\torganstamp{%
  \node[anchor=north east]
  at ([xshift=10pt,yshift=10pt]current page.north east)
  {
    \includegraphics[width=.11\paperwidth,angle=-30]
      {pictures/torgan/torgan.tampon.png}
  };
}

%% Onesheet specific commands
\@ifclassloaded{swade-onesheet}{
  %% Deadlands
  \if@deadlands
  \def\torganstamp{%
    \node[anchor=north east]
    at ([xshift=5pt,yshift=8pt]current page.north east)
    {
      \includegraphics[width=.11\paperwidth,angle=-20]
        {pictures/torgan/torgan.sheriff.png}
    };
  }
  \def\settinglogo{%
    \quad
    \raisebox{1.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]{pictures/logo-deadlands.png}
    }
  }
  \fi

  %% East Texas University
  \if@etu
  \def\torganstamp{%
    \node[anchor=north east]
      at ([xshift=10pt,yshift=10pt]current page.north east)
      {
        \includegraphics[width=.11\paperwidth,angle=-30]
          {pictures/torgan/torgan.tampon.png}
      };
    }
  \def\settinglogo{%
    \;
    \raisebox{.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]
      {pictures/logo-east_texas_university.png}
    }
  }
  \fi

  %% The Last Parsec
  \if@lastparsec
  \def\torganstamp{%
    \node[anchor=north east]
    at ([xshift=15pt,yshift=20pt]current page.north east)
    {
      \includegraphics[width=.145\paperwidth,angle=-20]
        {pictures/torgan/torgan.neon.png}
    };
  }
  \def\settinglogo{%
    \quad
    \raisebox{1.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]{pictures/logo-the_last_parsec.png}
    }
  }
  \fi

  % Specialize copyright notice
  \AtBeginDocument{%
    \renewcommand\thecopyrightnotice{%
      \begin{tikzpicture}[remember picture,overlay]
        \matrix[matrix of nodes,
                nodes={inner sep=5pt,outer sep=0,align=center},
                ampersand replacement=\&,
                anchor=center,
                inner sep=0,
                outer sep=0,
                minimum width=.3\paperwidth]
        at ([xshift=-10pt,yshift=.065\paperheight]current page.south) {%
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.25\paperwidth]
            {pictures/logo-pinnacle.png}\\
            \includegraphics[width=.085\paperwidth]
            {pictures/logo-bbe.png}
            \ifdefined\settinglogo
              \settinglogo
            \fi
          \end{minipage} \&
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.275\paperwidth]
            {pictures/torgan/torgan-savagerolistes-horizontal.png}
          \end{minipage} \&
          \begin{minipage}{.32\paperwidth}
            \small\sffamily\bfseries%
            Édition \emph{Adventure} Copyright \copyright\space 2020.\\
            \emph{Savage Worlds}%
            \ifdefined\thesettingname, \emph{\thesettingname}\fi\space%
            et tous les personnages, images et logos associés sont
            protégés par le droit d’auteur. \emph{Pinnacle Entertainment Group.}
            Tous droits réservés.
          \end{minipage} \\
        };
      \end{tikzpicture}
    }
  }

  %% Specialize \maketitle Command
  \renewcommand\maketitle{\par
    \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
      \hb@xt@1.8em{%
        \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \twocolumn[\@maketitle]%
    \@thanks
    \enlargethispage{-.1\paperheight}%
    \endgroup
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
  }
  \def\@maketitle{%
    \newpage
    \null
    \begin{onesheettitle}%
      \let \footnote \thanks
      \begin{minipage}{.685\paperwidth}
        \centering
        \HUGE\titlestyle%
        \textls[-100]{\@title}\par
      \end{minipage}
      \ifdefined\thesubtitle
      \vskip 1.25em
      \begin{minipage}{.85\paperwidth}
        \centering
        \large\sffamily\bfseries%
        \thesubtitle
      \end{minipage}
      \fi
    \end{onesheettitle}%
    \par%
    \vskip 1.5em%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint
      \node[anchor=south,inner sep=0,outer sep=0] at (current page.south)
      {
        \includegraphics[width=\paperwidth,height=.145\paperheight]
          {pictures/onesheet-bottom-banner.png}
      };
      \fi
      \node[anchor=north west]
      at ([xshift=2pt,yshift=-6pt]current page.north west)
      {
        \includegraphics[width=.15\paperwidth]
          {pictures/logo-swade-fan-product.png}
      };
      \torganstamp
    \end{tikzpicture}
    \thecopyrightnotice
    \afterpage{\enlargethispage{-.1\paperheight}}%
  }
}{}



\usetikzlibrary{shadings,decorations.text,shadows,fadings}
\tikzfading[name=fade out,
            inner color=transparent!25,
            outer color=transparent!100]


%% Article specific commands
\@ifclassloaded{swade-article}{%
  % Specialize copyright notice
  \AtBeginDocument{%
    \renewcommand\thecopyrightnotice{%
      \begin{tikzpicture}[remember picture,overlay]
        \matrix[matrix of nodes,
                nodes={inner sep=5pt,outer sep=0,align=center},
                ampersand replacement=\&,
                anchor=center,
                inner sep=0,
                outer sep=0,
                minimum width=.3\paperwidth]
        at ([xshift=-10pt,yshift=.065\paperheight]current page.south) {%
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.25\paperwidth]
            {pictures/logo-pinnacle.png}\\
            \includegraphics[width=.085\paperwidth]
            {pictures/logo-bbe.png}
            \ifdefined\settinglogo
              \settinglogo
            \fi
          \end{minipage} \&
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.275\paperwidth]
            {pictures/torgan/torgan-savagerolistes-horizontal.png}
          \end{minipage} \&
          \begin{minipage}{.32\paperwidth}
            \small\sffamily\bfseries%
            Édition \emph{Adventure} Copyright \copyright\space 2020.\\
            \emph{Savage Worlds}%
            \ifdefined\thesettingname, \emph{\thesettingname}\fi\space%
            et tous les personnages, images et logos associés sont
            protégés par le droit d’auteur. \emph{Pinnacle Entertainment Group.}
            Tous droits réservés.
          \end{minipage} \\
        };
      \end{tikzpicture}
    }
  }

  %%%%% Title page command %%%%%
  \if@titlepage
  \renewcommand\maketitle{%
    \begin{titlepage}%
      \let\footnotesize\small
      \let\footnoterule\relax
      \thispagestyle{empty}
      \null\vskip 5em%
      \begin{center}%
        \null\vfil
        \tikz[remember picture, overlay]{%
          % Title page background
          \node at (current page.center)
          {% Title page background
            \includegraphics[width=\paperwidth,height=\paperheight]
            {pictures/background-titlepage.jpg}
          };
          \node[yshift=.075\paperheight] at (current page.south)
          {% SWADE fan product logo
            \includegraphics[height=.125\paperheight]
            {pictures/logo-swade-fan-product.png}
          };
          % Document title
          \node[align=center,text width=.9\paperwidth,
          yshift=-.135\paperheight] (title)
          at (current page.north)
          {\titlestyle\MASSIVE\textcolor{white}{\textls[-100]{\@title}}\par};
          % Document's subtitle
          \ifdefined\thesubtitle
          \node[below=3em of title.south]
          {%
            \begin{minipage}{.85\paperwidth}%
              \centering%
              \Large\bfseries\sffamily\textcolor{white}{\thesubtitle}%
            \end{minipage}%
          };
          \fi
          % Setting logo
          \node[yshift=.235\paperheight] (settinglogo)
          at (current page.south)
          {\includegraphics[height=.15\paperheight]
            {pictures/torgan/torgan-savagerolistes-horizontal-white.png}};
          % Cover picture
          \ifdefined\thecoverpicture%
          \path[shift={(current page.south west)},
          fill overzoom image={\thecoverpicture}]
          (0,.337\paperheight) rectangle ++(\paperwidth,.296\paperheight) {};
          \fi
          % Torgan stamp
          \node[yshift=-.37\paperheight,xshift=-.1\paperwidth] (stamp)
          at (current page.north east)
          {\includegraphics[height=.215\paperheight]
            {pictures/torgan/torgan.cachet.png}};
        }
        \vskip 200pt
      \end{center}\par
      \newpage
      \thispagestyle{empty}
      \par\null
      \vfill
      \par\null
      \thecredits
      \par
    \end{titlepage}%
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
  }
  \else
  \renewcommand\maketitle{\par
    \begingroup
    \thispagestyle{empty}
    \pagestyle{mainmatter}
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
      \hb@xt@1.8em{%
        \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \if@twocolumn
    \ifnum \col@number=\@ne
    \@maketitle
    \else
    \twocolumn[\@maketitle]%
    \fi
    \else
    \newpage
    \global\@topnum\z@ % Prevents figures from going at top of page.
    \@maketitle
    \fi
    \@thanks
    \enlargethispage{-.075\paperheight}%
    \endgroup
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
  }
\def\@maketitle{%
  \newpage
  \null
  \tikz[remember picture,overlay]{%
    \node(chapterbox)[anchor=north,inner sep=0,outer sep=0]
    at (current page.north)
    {%
      \begin{minipage}{.85\paperwidth}
        \begin{chapterbox}%
          \let\footnote\thanks
          \begin{minipage}{\linewidth}
            \setlength{\spaceskip}{14pt plus 18pt}%
            \centering
            {\titlestyle\HUGE\textls[-100]{\@title}\par}%
          \end{minipage}
        \end{chapterbox}%
        \ifdefined\thesubtitle
        \vskip -.75em%
        \centering
        {\sffamily\large\bfseries
          \begin{tabular}[t]{c}%
            \thesubtitle
          \end{tabular}
        }%
        \fi
      \end{minipage}
     };
     \pgfheight{\chapterboxheight}
   }
  \addtolength{\chapterboxheight}
              {\dimexpr(-1in-\voffset-\topmargin-\headheight-\headsep)}
  \vskip \chapterboxheight
  \par%
  \thecopyrightnotice
  \afterpage{\enlargethispage{-.075\paperheight}}%
}
\fi
}{}

\@ifclassloaded{swade-book}{%
  %% Deadlands
  \if@deadlands
  \def\torganstamp{%
    \node[yshift=-.37\paperheight,xshift=-.115\paperwidth] (stamp)
    at (current page.north east)
    {\includegraphics[height=.15\paperheight,angle=-20]
      {pictures/torgan/torgan.sheriff.png}
    };
  }
  \def\settinglogo{%
    \quad
    \raisebox{1.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]{pictures/logo-deadlands.png}
    }
  }
  \fi

  %% East Texas University
  \if@etu
  \def\torganstamp{%
    \node[yshift=-.375\paperheight,xshift=-.1\paperwidth] (stamp)
    at (current page.north east)
    {\includegraphics[height=.125\paperheight,angle=-40]
      {pictures/torgan/torgan.tampon.png}
    };
  }
  \def\settinglogo{%
    \;
    \raisebox{.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]
      {pictures/logo-east_texas_university.png}
    }
  }
  \fi

  %% The Last Parsec
  \if@lastparsec
  \def\torganstamp{%
    \node[yshift=-.37\paperheight,xshift=-.1\paperwidth] (stamp)
    at (current page.north east)
    {\includegraphics[height=.15\paperheight,angle=-20]
      {pictures/torgan/torgan.neon.png}
    };
  }
  \def\settinglogo{%
    \;
    \raisebox{1.25em}[0pt][0pt]{%
      \includegraphics[width=.175\paperwidth]{pictures/logo-the_last_parsec.png}
    }
  }
  \fi

  % Specialize copyright notice
  \AtBeginDocument{%
    \renewcommand\thecopyrightnotice{%
      \begin{tikzpicture}[remember picture,overlay]
        \matrix[matrix of nodes,
                nodes={inner sep=5pt,outer sep=0,align=center},
                ampersand replacement=\&,
                anchor=center,
                inner sep=0,
                outer sep=0,
                minimum width=.3\paperwidth]
        at ([xshift=-10pt,yshift=.065\paperheight]current page.south) {%
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.25\paperwidth]
            {pictures/logo-pinnacle.png}\\
            \includegraphics[width=.085\paperwidth]
            {pictures/logo-bbe.png}
            \ifdefined\settinglogo
              \settinglogo
            \fi
          \end{minipage} \&
          \begin{minipage}{.3\paperwidth}
            \centering
            \includegraphics[width=.275\paperwidth]
            {pictures/torgan/torgan-savagerolistes-horizontal.png}
          \end{minipage} \&
          \begin{minipage}{.32\paperwidth}
            \small\sffamily\bfseries%
            Édition \emph{Adventure} Copyright \copyright\space 2020.\\
            \emph{Savage Worlds}%
            \ifdefined\thesettingname, \emph{\thesettingname}\fi\space%
            et tous les personnages, images et logos associés sont
            protégés par le droit d’auteur. \emph{Pinnacle Entertainment Group.}
            Tous droits réservés.
          \end{minipage} \\
        };
      \end{tikzpicture}
    }
  }

  %%%%% Title page command %%%%%
  \renewcommand\maketitle{%
    \begin{titlepage}%
      \let\footnotesize\small
      \let\footnoterule\relax
      \thispagestyle{empty}
      \null\vskip 5em%
      \begin{center}%
        \null\vfil
        \tikz[remember picture, overlay]{%
          % Title page background
          \node at (current page.center)
          {% Title page background
            \includegraphics[width=\paperwidth,height=\paperheight]
            {pictures/background-titlepage.jpg}
          };
          \node[yshift=.075\paperheight] at (current page.south)
          {% SWADE fan product logo
            \includegraphics[height=.125\paperheight]
            {pictures/logo-swade-fan-product.png}
          };
          % Document title
          \node[align=center,text width=.9\paperwidth,
          yshift=-.135\paperheight] (title)
          at (current page.north)
          {\titlestyle\MASSIVE\textcolor{white}{\textls[-100]{\@title}}\par};
          % Document's subtitle
          \ifdefined\thesubtitle
          \node[below=3em of title.south]
          {%
            \begin{minipage}{.85\paperwidth}%
              \centering%
              \Large\bfseries\sffamily\textcolor{white}{\thesubtitle}%
            \end{minipage}%
          };
          \fi
          % Setting logo
          \node[yshift=.235\paperheight] (settinglogo)
          at (current page.south)
          {\includegraphics[height=.15\paperheight]
            {pictures/torgan/torgan-savagerolistes-horizontal-white.png}};
          % Cover picture
          \ifdefined\thecoverpicture%
          \path[shift={(current page.south west)},
          fill overzoom image={\thecoverpicture}]
          (0,.337\paperheight) rectangle ++(\paperwidth,.296\paperheight) {};
          \fi
          % Torgan stamp
          \torganstamp
        }
        \vskip 200pt
      \end{center}\par
      \newpage
      \thispagestyle{empty}
      \par\null
      \vfill
      \par\null
      \thecredits
      \par
    \end{titlepage}%
    \setcounter{footnote}{0}%
    \global\let\thanks\relax
    \global\let\maketitle\relax
    \global\let\@thanks\@empty
    \global\let\@author\@empty
    \global\let\@date\@empty
    \global\let\@title\@empty
    \global\let\title\relax
    \global\let\author\relax
    \global\let\date\relax
    \global\let\and\relax
  }
}{}
