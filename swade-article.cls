%%
%% This file provide a LaTeX document class for articles written by
%% fans of the Savage Worlds RPG. This is an open-source work, so feel
%% free to share it and to modify it according to the license below.
%%
%% -------------------------------------------------------------------
%%
%% Copyright (C) 2021, Emmanuel Fleury <emmanuel.fleury@gmail.com>
%%
%% Permission to use, copy, modify, and/or distribute this software
%% for any purpose with or without fee is hereby granted.
%%
%% THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
%% WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
%% AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
%% CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
%% LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
%% NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
%% CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
%%

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{swade-article}[2021/01/01 v0.4
SWADE article document class]

\newif\if@noprint       \@noprinttrue
\newif\if@titlepage     \@titlepagefalse
\newif\if@landscape     \@landscapefalse

\def\swade@fontsize{}
\def\swade@papersize{}

% Class options
\DeclareOption{a4paper}{\def\swade@papersize{a4paper}}
\DeclareOption{a5paper}{\def\swade@papersize{a5paper}}
\DeclareOption{letterpaper}{\def\swade@papersize{letterpaper}}

\DeclareOption{8pt}{\def\swade@fontsize{8pt}}
\DeclareOption{9pt}{\def\swade@fontsize{9pt}}
\DeclareOption{10pt}{\def\swade@fontsize{10pt}}
\DeclareOption{11pt}{\def\swade@fontsize{11pt}}
\DeclareOption{12pt}{\def\swade@fontsize{12pt}}

\DeclareOption{landscape}{\@landscapetrue}
\DeclareOption{titlepage}{\@titlepagetrue}
\DeclareOption{print}{\@noprintfalse}

\ProcessOptions

\PassOptionsToClass{\swade@papersize}{extarticle}
\PassOptionsToClass{\swade@fontsize}{extarticle}

\if@landscape
  \PassOptionsToClass{landscape}{extarticle}
\fi
\if@titlepage
  \PassOptionsToClass{titlepage}{extarticle}
\fi

\LoadClass[twocolumn,twoside]{extarticle}

%%%%% Loading common swade utilities %%%%%
\input{swade-common.ltx}

%% Setting geometry
\RequirePackage[driver=pdftex,
                textwidth=.85\paperwidth,
                columnsep=16pt,
                top=.05\paperheight,
                bottom=.075\paperheight]{geometry}

%% Get fancyhdr package
\RequirePackage{fancyhdr}

%% Extracting current section title
\global\let\thesectiontitle\@empty
\apptocmd{\ttl@straight@i}{\update@thesectiontitle{#1}}{}{}
\def\update@thesectiontitle#1{%
  \ifnum\pdf@strcmp{#1}{section}=0
    \global\let\thesectiontitle\ttl@savetitle
  \fi
}


%% Fancy header settings
\newlength\bannerwidth
\setlength\bannerwidth{4em}

\newlength\topevenbannerheight
\if@landscape
\setlength\topevenbannerheight{.5\paperheight}
\else
\setlength\topevenbannerheight{.4\paperheight}
\fi

\newlength\topoddbannerheight
\if@landscape
\setlength\topoddbannerheight{.425\paperheight}
\else
\setlength\topoddbannerheight{.375\paperheight}
\fi


% Used to increment the \topbanneroffset of odd-side banner at each \part
\newlength\topbanneroffsetincr
\setlength\topbanneroffsetincr{.05\paperheight}
\newlength\topbanneroffset
\setlength\topbanneroffset{\topbanneroffsetincr}


%% Title page lengths
\newlength\titleheadheight
\setlength\titleheadheight{.45\paperheight}
\newlength\titleheadwidth
\setlength\titleheadwidth{.885\paperwidth}


%%%%% Pages background images %%%%%
\newcommand\AtPageUpperRight[1]{\AtPageUpperLeft{%
 \put(\LenToUnit{\paperwidth},\LenToUnit{0\paperheight}){#1}%
 }}%
\newcommand\AtPageLowerRight[1]{\AtPageLowerLeft{%
 \put(\LenToUnit{\paperwidth},\LenToUnit{0\paperheight}){#1}%
 }}%


\if@noprint
\AddToShipoutPictureBG{%
  \ifnumodd{\thepage}
    {%
    \begin{ocg}{Background}{bckgrd}{1}
      \includegraphics[width=\paperwidth,%
                       height=\paperheight]{pictures/background-odd.jpg}
    \end{ocg}
    }%
    {%
    \begin{ocg}{Background}{bckgrd}{1}
      \includegraphics[width=\paperwidth,%
                       height=\paperheight]{pictures/background-even.jpg}
    \end{ocg}
    }%
  }
\fi

\fancypagestyle{empty}{%
  \fancyhf{} % Clear previous settings
  \renewcommand{\headrulewidth}{0pt}
}

\fancypagestyle{plain}{%
  \fancyhf{} % Clear previous settings
  \renewcommand{\headrulewidth}{0pt}

  %% Fancy footer settings
  \fancyfoot[LE]{%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint%
      \node[anchor=south west,inner sep=0,outer sep=0]
        at (current page.south west)
          {\includegraphics[width=\bannerwidth]{pictures/even-page-marker.png}};
      \fi
      \node[inner sep=0,outer sep=0]
      at ([xshift=.45\bannerwidth,
           yshift=.045\paperheight]current page.south west)
      {\LARGE\sffamily\bfseries\textcolor{\structurefg}{\thepage}};
    \end{tikzpicture}
  }
  \fancyfoot[RO]{%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint%
      \node[anchor=south east,inner sep=0,outer sep=0]
        at (current page.south east)
          {\includegraphics[width=\bannerwidth]{pictures/odd-page-marker.png}};
      \fi
      \node[inner sep=0,outer sep=0]
      at ([xshift=-.5\bannerwidth,
           yshift=.045\paperheight]current page.south east)
      {\LARGE\sffamily\bfseries\textcolor{\structurefg}{\thepage}};
    \end{tikzpicture}
  }
}

\fancypagestyle{mainmatter}{%
  \fancyhf{} % Clear previous settings
  \renewcommand{\headrulewidth}{0pt}

  %% Fancy left even sidebar
  \fancyhead[LE]{
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint
      \node[anchor=north west,inner sep=0,outer sep=0]
      at (current page.north west)
      {\includegraphics[width=\bannerwidth,
                        height=\topevenbannerheight]
         {pictures/even-side-banner.png}};
      \fi
      \node[inner sep=0,outer sep=0,rotate=90]
      at ([xshift=.5\bannerwidth,
           yshift=-.5\topevenbannerheight]current page.north west)
      {\begin{minipage}{.885\topevenbannerheight}
          \large\bfseries\sffamily\scshape%
          \textcolor{\structurefg}{\thetitlename}
        \end{minipage}};
    \end{tikzpicture}
  }

  %% Fancy right odd sidebar
  \fancyhead[RO]{%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint%
      \node[anchor=north east,inner sep=0,outer sep=0]
      at ([yshift=-\topbanneroffset]current page.north east)
      {\includegraphics[width=\bannerwidth,
                        height=\topoddbannerheight]{pictures/odd-side-banner.png}};
      \fi
      \node[inner sep=0,outer sep=0,rotate=-90]
      at ([xshift=-.5\bannerwidth,
           yshift=-\topbanneroffset-.5\topoddbannerheight]current page.north east)
        {\begin{minipage}{.885\topoddbannerheight}%
            \let\\=\relax % ignore \newline within section title (if any)
            \large\bfseries\sffamily\scshape%
            \textcolor{\structurefg}{\hfill\thesectiontitle}%
         \end{minipage}};
     \end{tikzpicture}
  }

  %% Fancy footer settings
  \fancyfoot[LE]{%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint%
      \node[anchor=south west,inner sep=0,outer sep=0]
        at (current page.south west)
          {\includegraphics[width=\bannerwidth]{pictures/even-page-marker.png}};
      \fi
      \node[inner sep=0,outer sep=0]
      at ([xshift=.45\bannerwidth,
           yshift=.045\paperheight]current page.south west)
      {\LARGE\sffamily\bfseries\textcolor{\structurefg}{\thepage}};
    \end{tikzpicture}
  }
  \fancyfoot[RO]{%
    \begin{tikzpicture}[remember picture,overlay]
      \if@noprint%
      \node[anchor=south east,inner sep=0,outer sep=0]
        at (current page.south east)
          {\includegraphics[width=\bannerwidth]{pictures/odd-page-marker.png}};
      \fi
      \node[inner sep=0,outer sep=0]
      at ([xshift=-.5\bannerwidth,
           yshift=.045\paperheight]current page.south east)
      {\LARGE\sffamily\bfseries\textcolor{\structurefg}{\thepage}};
    \end{tikzpicture}
  }
}

%% Setting default page style
\AtBeginDocument{%
  \pagestyle{mainmatter}
}


%%%%% Specific to swade-article class %%%%%

%%% Redefining sections titles
\titleformat{\section}[block]
            {\normalfont\LARGE\bfseries\sffamily}
            {}
            {0em}
            {\relax\vspace{-1.85em}%
              \begin{savagesection}{}%
                \centering%
                \textcolor{darkred}{\textls[-50]{\MakeUppercase{#1}}}%
              \end{savagesection}%
              \vspace{-1em}\relax}
\titlespacing*{\section}
{0pt}{1.5ex plus .75ex minus .5ex}{.25ex}

\titleformat{\subsection}
            {\normalfont\Large\bfseries\sffamily}
            {}
            {0em}
            {\MakeUppercase{\textls[-50]{#1}}}
            [{\titlerule[1pt]}]
\titlespacing*{\subsection}
{0pt}{2.25ex plus 1ex minus .25ex}{1.5ex plus .25ex}

\titleformat{\subsubsection}
            {\normalfont\large\bfseries\sffamily}
            {}
            {0em}
            {\MakeUppercase{\textls[-50]{#1}}}
\titlespacing*{\subsubsection}
{0pt}{2.25ex plus 1ex minus .25ex}{1.5ex plus .25ex}

\titleformat{\paragraph}
            {\normalfont\normalsize\bfseries\sffamily\scshape}
            {}
            {0em}
            {#1}
\titlespacing*{\paragraph}{0pt}{.65ex plus .25ex minus .25ex}{0em}

%% Table of contents
\setcounter{tocdepth}{2}

\titlecontents{section}[0em]
              {\sffamily\scshape}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}

\titlecontents{subsection}[1em]
              {\sffamily}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}

\titlecontents{subsubsection}[3em]
              {\sffamily}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}

%%% Chapter header environment
\newtcolorbox{chapterbox}[1][]{%
  enhanced jigsaw, blankest,
  watermark graphics={pictures/chapter-oddside-title.png},
  watermark stretch=1.00,
  opacityback=0,
  before upper={\vskip -2em},
  halign=left,
  valign=center,
  height=.325\paperheight,
  height plus=.5\paperheight,
  boxrule=0pt,
  left=25pt,
  right=25pt,
  top=.125\paperheight,
  bottom=.075\paperheight,
  {#1}
}

% Calculate chapterbox height to skip this space
\newlength\chapterboxheight
\newcommand{\pgfheight}[1]{%
  \pgfextracty{\@tempdima}{\pgfpointdiff{\pgfpointanchor{chapterbox}{south}}
                          {\pgfpointanchor{chapterbox}{north}}}
  \global#1=\@tempdima
}

%% Redefining various command because of the titlepage option
\renewenvironment{titlepage}
{%
  \if@twocolumn
    \@restonecoltrue\onecolumn
  \else
    \@restonecolfalse\newpage
  \fi
}%
{\if@restonecol\twocolumn\else\newpage\fi}

\renewenvironment{abstract}{%
  \if@twocolumn
    \section*{\abstractname}%
  \else\small
    \begin{center}%
      {\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
    \end{center}%
  \quotation
  \fi
}
{\if@twocolumn\else\endquotation\fi}

%%%%% Title page command %%%%%
\if@titlepage
\renewcommand\maketitle{%
  \begin{titlepage}%
    \let\footnotesize\small
    \let\footnoterule\relax
    \thispagestyle{empty}
    \null\vskip 5em%
    \begin{center}%
      \null\vfil
      \tikz[remember picture, overlay]{%
        % Title page background
        \node at (current page.center)
        {% Title page background
          \includegraphics[width=\paperwidth,height=\paperheight]
          {pictures/background-titlepage.jpg}
        };
        \node[yshift=.15\paperheight] at (current page.south)
        {% SWADE fan product logo
          \includegraphics[height=.175\paperheight]
          {pictures/logo-swade-fan-product.png}
        };
        % Document title
        \node[align=center,text width=.85\paperwidth,
        draw,fill=black,opacity=.25,rounded corners,inner sep=10pt,
        xshift=4pt,yshift=.325\paperheight-2pt] (title)
        at (current page.center)
        {\titlestyle\HUGE\textcolor{black}{\textls[-100]{\@title}}\par};
        \node[align=center,text width=.85\paperwidth,
        yshift=.325\paperheight] (title)
        at (current page.center)
        {\titlestyle\HUGE\textcolor{white}{\textls[-100]{\@title}}\par};
        % Document's subtitle
        \ifdefined\thesubtitle
        \node[below=4em of title.south,xshift=2pt,yshift=-1pt]
        {\large\bfseries\sffamily\textcolor{black}{\thesubtitle}};
        \node[below=4em of title.south]
        {\large\bfseries\sffamily\textcolor{white}{\thesubtitle}};
        \fi
        % Cover picture
        \ifdefined\thecoverpicture%
        \path[shift={(current page.south west)},
        fill overzoom image={\thecoverpicture}]
        (0,.337\paperheight) rectangle ++(\paperwidth,.296\paperheight) {};
        \fi
      }
      \vskip 200pt
    \end{center}\par
    \newpage
    \thispagestyle{empty}
    \par\null
    \vfill
    \par\null
    \thecredits
    \par
  \end{titlepage}%
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\else
\renewcommand\maketitle{\par
  \begingroup
  \thispagestyle{empty}
  \pagestyle{mainmatter}
  \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
  \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
  \long\def\@makefntext##1{\parindent 1em\noindent
    \hb@xt@1.8em{%
      \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
  \if@twocolumn
  \ifnum \col@number=\@ne
  \@maketitle
  \else
  \twocolumn[\@maketitle]%
  \fi
  \else
  \newpage
  \global\@topnum\z@ % Prevents figures from going at top of page.
  \@maketitle
  \fi
  \@thanks
  \enlargethispage{-.075\paperheight}%
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \newpage
  \null
  \tikz[remember picture,overlay]{%
    \node(chapterbox)[anchor=north,inner sep=0,outer sep=0]
    at (current page.north)
    {%
      \begin{minipage}{.85\paperwidth}
        \begin{chapterbox}%
          \let\footnote\thanks
          \begin{minipage}{\linewidth}
            \setlength{\spaceskip}{14pt plus 18pt}%
            \centering
            {\titlestyle\HUGE\textls[-100]{\@title}\par}%
          \end{minipage}
        \end{chapterbox}%
        \ifdefined\thesubtitle
        \vskip -.75em%
        \centering
        {\sffamily\Large
          \begin{tabular}[t]{c}%
            \thesubtitle
          \end{tabular}
        }%
        \fi
      \end{minipage}
     };
     \pgfheight{\chapterboxheight}
   }
  \addtolength{\chapterboxheight}
              {\dimexpr(-1in-\voffset-\topmargin-\headheight-\headsep)}
  \vskip\chapterboxheight
  \par%
  \thecopyrightnotice
  \afterpage{\enlargethispage{-.075\paperheight}}%
}
\fi

\endinput
