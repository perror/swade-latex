%%
%% This file provide a LaTeX document class for articles written by
%% fans of the Savage Worlds RPG. This is an open-source work, so feel
%% free to share it and to modify it according to the license below.
%%
%% -------------------------------------------------------------------
%%
%% Copyright (C) 2021, Emmanuel Fleury <emmanuel.fleury@gmail.com>
%%
%% Permission to use, copy, modify, and/or distribute this software
%% for any purpose with or without fee is hereby granted.
%%
%% THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
%% WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
%% AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
%% CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
%% LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
%% NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
%% CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
%%

%% Package requiring to be loaded first
\RequirePackage[table]{xcolor}

%% Font packages
\RequirePackage{lmodern}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{palatino}
\RequirePackage{amssymb}

\RequirePackage[expansion=true,kerning=true,%
                protrusion=true,spacing=true,%
                tracking=alltext,letterspace=-30]{microtype}
%\microtypecontext{spacing=nonfrench}

%% Miscellaneous packages
\RequirePackage{afterpage}
\RequirePackage{enumitem}
\RequirePackage{etoolbox}
\RequirePackage{ocgx2}
\RequirePackage{tabularx}
\RequirePackage{xparse}
\RequirePackage{xspace}

% Loading the hypperref package
\RequirePackage[hidelinks,pdfusetitle]{hyperref}

%% Redefining headers, footers and section headers
\RequirePackage[explicit]{titlesec}
\RequirePackage{titletoc}

%% Graphics
\RequirePackage{eso-pic}
\RequirePackage{graphicx}

\RequirePackage[most]{tcolorbox}
\tcbuselibrary{skins}

\RequirePackage{tikz} %% Tikz is last to avoid conflicts
\usetikzlibrary{%
  calc,
  decorations.markings,
  decorations.pathmorphing,
  matrix,
  positioning,
  shadings,
  shadows,
  shadows.blur,
  shapes}

% \HUGE font command
\newcommand\HUGE{%
  \@setfontsize\HUGE{40}{\baselineskip}\strut%
  \spaceskip=.75\fontdimen2\font plus .25\fontdimen3\font%
                                minus .25\fontdimen4\font%
}

% \MASSIVE font command
\newcommand\MASSIVE{%
  \@setfontsize\MASSIVE{60}{\baselineskip}\strut%
  \spaceskip=.75\fontdimen2\font plus .25\fontdimen3\font%
                                minus .25\fontdimen4\font%
}

% Working on harmonious spacing
\setlength{\parskip}{1px plus 1px minus .75px}

% Store parindent spaces
\newlength\savedparindent
\setlength\savedparindent\parindent


%%%%% Babel specific translations %%%%%
\newcommand\agilityname{Agility}
\newcommand\smartsname{Smarts}
\newcommand\spiritname{Spirit}
\newcommand\strengthname{Strength}
\newcommand\vigorname{Vigor}

\newcommand\pacename{Pace}
\newcommand\parryname{Parry}
\newcommand\toughnessname{Toughness}

\newcommand\skillsname{Skills}
\newcommand\edgesname{Edges}
\newcommand\hindrancesname{Hindrances}
\newcommand\cyberwarename{Cyberware}
\newcommand\languagesname{Languages}

\newcommand\datename{Date}
\newcommand\versionname{Version}
\newcommand\authorname{Author}

\newcommand\colonspace{\relax}%

\newcommand\attributesarray{%
  \begin{tabularx}{\textwidth}{CCCPC}
    \hline
    \textbf{\agilityname} & \textbf{\smartsname} & \textbf{\spiritname} & \textbf{\strengthname} & \textbf{\vigorname} \\
    \theagility & \thesmarts & \thespirit & \thestrength & \thevigor \\
    \hline
  \end{tabularx}
}

% Credits
\newcommand\creditsname{Credits}
\newcommand\authorfieldtitle{Writer}
\newcommand\datefieldtitle{Date}
\newcommand\versionfieldtitle{Version}
\newcommand\thecredits{%
  \ifdefined\theauthor
  \begin{minipage}{.95\textwidth}
    \section*{\creditsname}
    \begin{itemize}[label={},leftmargin=.5pt]\large
    \item \textbf{\large\sffamily\textsc{\authorfieldtitle} :} \theauthor
      \ifdefined\thedate
    \item \textbf{\large\sffamily\textsc{\datefieldtitle} :} \thedate
      \fi
      \ifdefined\theversion
    \item \textbf{\large\sffamily\textsc{\versionfieldtitle} :} \theversion
      \fi
    \end{itemize}
  \end{minipage}
  \par\vskip 10em
  \fi
  {\thecopyrightnotice}\par
}

\AtBeginDocument{%
  \@ifpackageloaded{babel}{%
    %%% French
    \addto{\extrasfrench}{%
      % Custom setting
      \renewcommand\colonspace{\,}%
      \renewcommand\contentsname{Sommaire}%
      \renewcommand\creditsname{Crédits}
      \renewcommand\authorfieldtitle{Écriture}
      % Required settings
      \renewcommand\agilityname{Agilité}%
      \renewcommand\smartsname{Intellect}%
      \renewcommand\spiritname{Âme}%
      \renewcommand\strengthname{Force}%
      \renewcommand\vigorname{Vigeur}%
      \renewcommand\attributesarray{%
        \begin{tabularx}{\textwidth}{CCCCC}
          \hline
          \textbf{\agilityname} & \textbf{\spiritname} & \textbf{\strengthname} & \textbf{\smartsname} & \textbf{\vigorname} \\
          \theagility & \thespirit & \thestrength & \thesmarts & \thevigor \\
          \hline
        \end{tabularx}
       }
      \renewcommand\pacename{Allure}%
      \renewcommand\parryname{Parade}%
      \renewcommand\toughnessname{Résistance}%
      \renewcommand\skillsname{Compétences}%
      \renewcommand\edgesname{Atouts}%
      \renewcommand\hindrancesname{Handicaps}%
      \renewcommand\cyberwarename{Cyberware}%
      \renewcommand\languagesname{Langages}%
      \renewcommand\authorname{Auteur}%
      \renewcommand\thecopyrightnotice{%
  \begin{tikzpicture}[remember picture,overlay]
   \matrix[matrix of nodes,
           nodes={inner sep=5pt,outer sep=0,align=center},
           ampersand replacement=\&,
           anchor=center,
           inner sep=0,outer sep=0,
           minimum width=.3\paperwidth]
     at ([xshift=-10pt,yshift=.065\paperheight]current page.south) {%
       \begin{minipage}{.3\paperwidth}
         \centering
         \if@landscape
           \includegraphics[height=.115\paperheight]
             {pictures/logo-swade-fan-product.png}
         \else
           \includegraphics[width=.25\paperwidth]
             {pictures/logo-swade-fan-product.png}
         \fi
       \end{minipage}\&
       \begin{minipage}{.3\paperwidth}
         \centering
         \if@landscape
           \includegraphics[height=.1\paperheight]
             {pictures/logo-pinnacle.png}
         \else
           \includegraphics[width=.275\paperwidth]
             {pictures/logo-pinnacle.png}
         \fi
       \end{minipage}\&
       \begin{minipage}{.32\paperwidth}
         \small\sffamily\bfseries%
         Édition Adventure Copyright \copyright\space 2020.\\
         \emph{Savage Worlds} et tous les personnages, images et logos
         associés sont protégés par le droit d’auteur.
         Pinnacle Entertainment Group.\\
         Tous droits réservés.
       \end{minipage}\\
     };
   \end{tikzpicture}}%
    }
  }{}
}


%% Colors definitions
\definecolor{darkbrown}{HTML}{dcd2b8}
\definecolor{lightbrown}{HTML}{eeeadf}
\definecolor{darkred}{HTML}{94070a} % SWADE Structure color

\if@noprint
\def\structure{darkred}
\def\structurefg{white}
\def\quotebg{lightbrown}
\def\oddrow{lightbrown}
\def\evenrow{darkbrown}
\def\headrowbg{black}
\def\headrowfg{white}
%
\else % 'print == true' going for B&W!
%
\def\structure{white}
\def\structurefg{black}
\def\quotebg{gray!15}
\def\evenrow{white}
\def\oddrow{gray!15}
\def\headrowbg{black}
\def\headrowfg{white}
\fi
%% Set the structure color: \setcolor{red!50!black}
\newcommand\setcolor[1]{\def\structure{#1}}

%% Set title style
\def\titlestyle{\sffamily\scshape\bfseries}

%%%%% Defining Title Commands %%%%%
\newcommand\subtitle[1]{\def\thesubtitle{#1}}
\newcommand\version[1]{\def\theversion{#1}}
\newcommand\coverpicture[1]{\def\thecoverpicture{#1}}

\newcommand\copyrightnotice[1]{\def\thecopyrightnotice{#1}}
\newcommand\thecopyrightnotice{
  \begin{tikzpicture}[remember picture,overlay]
   \matrix[matrix of nodes,
            nodes={inner sep=5pt,outer sep=0,align=center},
            ampersand replacement=\&,
            anchor=center,
            inner sep=0,outer sep=0,
            minimum width=.3\paperwidth]
        at ([xshift=-10pt,yshift=.065\paperheight]current page.south)
      {
        \begin{minipage}{.3\paperwidth}
          \centering
          \if@landscape
            \includegraphics[height=.115\paperheight]
              {pictures/logo-swade-fan-product.png}
          \else
            \includegraphics[width=.25\paperwidth]
              {pictures/logo-swade-fan-product.png}
          \fi
        \end{minipage}\&

        \begin{minipage}{.3\paperwidth}
          \centering
          \if@landscape
            \includegraphics[height=.1\paperheight]
              {pictures/logo-pinnacle.png}
          \else
            \includegraphics[width=.275\paperwidth]
              {pictures/logo-pinnacle.png}
          \fi
        \end{minipage}\&

        \begin{minipage}{.32\paperwidth}
          \small\sffamily\bfseries%
          Adventure Edition \copyright\space 2020.\\
          Savage Worlds, Logos, and the Pinnacle logo are
          \copyright\space 2019 Great White Games, LLC;
          DBA Pinnacle Entertainment Group.\\
          \url{http://www.peginc.com}%
        \end{minipage}\\
      };
  \end{tikzpicture}
}


% Save document's title in \thetitle
\newcommand{\@currenttitle}{\ttl@savetitle}
\AtBeginDocument{%
  \let\theauthor\@author
  \let\thedate\@date
  \let\thetitlename\@title
}


% Table style
\newcommand\setrow[1]{\gdef\rowmac{#1}#1\ignorespaces}
\newcommand\clearrow{\global\let\rowmac\relax}
\clearrow

\newcommand\headrow{%
  \rowcolor{\headrowbg}%
  \global\rownum=0\relax%
  \setrow{\titlestyle\leavevmode\color{\headrowfg}}%
}

%% Centered
\newcolumntype{C}{>{\rowmac\centering\let\newline\\\arraybackslash\hspace{0pt}}X}
\newcolumntype{P}{>{\rowmac\centering\let\newline\\\arraybackslash\hspace{0pt}}c}

%% Left aligned
\newcolumntype{L}{>{\rowmac\raggedright\let\newline\\\arraybackslash\hspace{0pt}}X}

%% Right aligned
\newcolumntype{R}{>{\rowmac\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}X}

\if@noprint
\def\tabletitlebg{\structure}
\def\tabletitlefg{\structurefg}
\def\tablebackground{lightbrown}
\def\tablehighlight{darkbrown}
\else
\def\tabletitlebg{\structurefg}
\def\tabletitlefg{\structure}
\def\tablebackground{lightgray!25}
\def\tablehighlight{lightgray}
\fi

\newtcolorbox{savagetable}[2]{%
  enhanced jigsaw,
  clip upper,
  arc=7pt,
  coltitle=\tabletitlefg,
  colbacktitle=\tabletitlebg,
  fonttitle=\large\titlestyle,
  before skip=7pt plus 2pt minus 2pt,
  center title,
  title={#2},
  tabularx*={\rowcolors{1}{\oddrow}{\evenrow}}{#1<{\clearrow}},
  boxrule=0pt
}

\NewTColorBox{savagetable*}{ O{!htbp} m m }{%
  float*,
  floatplacement={#1},
  width=\textwidth,
  enhanced jigsaw,
  clip upper,
  arc=7pt,
  coltitle=\tabletitlefg,
  colbacktitle=\tabletitlebg,
  fonttitle=\large\titlestyle,
  before skip=7pt plus 2pt minus 2pt,
  center title,
  title=#3,
  tabularx*={\rowcolors{1}{\oddrow}{\evenrow}}{#2<{\clearrow}},
  boxrule=0pt
}


%%%%% Statblock Environments %%%%%

\newcommand\fightglyph{%
  \raisebox{-.1em}[0pt][0pt]{%
    \includegraphics[height=.75\baselineskip]{glyphs/sword.png}\!\!\!
  }
}
\newcommand\fight[1]%
  {\item[\fightglyph] \textbf{\sffamily\itshape #1\colonspace:}}

\newcommand\shootglyph{%
  \raisebox{-.1em}[0pt][0pt]{%
    \includegraphics[height=.75\baselineskip]{glyphs/bow.png}\!\!\!
  }
}
\newcommand\shoot[1]%
  {\item[\shootglyph] \textbf{\sffamily\itshape #1\colonspace:}}

\newcommand\abilityglyph{%
  \raisebox{-.1em}[0pt][0pt]{%
    \includegraphics[height=.85\baselineskip]{glyphs/spark.png}\!\!\!\!\!
  }
}
\NewDocumentCommand\ability{s m}{%
  \IfBooleanTF#1%
  {\item[\abilityglyph] \textbf{\sffamily\itshape #2.}\space}%
  {\item[\abilityglyph] \textbf{\sffamily\itshape #2\colonspace:}}%
}

\newcommand\powerglyph{%
  \raisebox{-.1em}[0pt][0pt]{%
    \includegraphics[height=.75\baselineskip]{glyphs/lightning.png}\!\!\!
  }
}
\newcommand\power[1]{\item[\powerglyph] \textbf{\sffamily\itshape #1\colonspace:}}

\newcommand\otherglyph{%
  \raisebox{-.1em}[0pt][0pt]{%
    \includegraphics[height=.75\baselineskip]{glyphs/fire.png}\!\!\!
  }
}
\newcommand\other[1]{\item[\otherglyph] \textbf{\sffamily\itshape #1\colonspace:}}

\newcommand\modifier[1]{\item[\Large\bfseries +\!] \textbf{\sffamily\itshape #1\colonspace:}}

%% Other specific glyphs
\if@noprint
\newcommand\jokerglyph{%
  \raisebox{-.5em}[0pt][0pt]{%
    \includegraphics[height=1.35\baselineskip]{glyphs/joker-red_and_white.png}\!\!
  }
}
\else
\newcommand\jokerglyph{%
  \raisebox{-.5em}[0pt][0pt]{%
    \includegraphics[height=1.35\baselineskip]{glyphs/joker-black_and_white.png}\!\!
  }
}
\fi

\if@noprint
\newcommand\jokeritemglyph{%
  \raisebox{-.25em}[0pt][0pt]{%
    \includegraphics[height=\baselineskip]{glyphs/joker-red.png}\!\!\!\!
  }
}
\else
\newcommand\jokeritemglyph{%
  \raisebox{-.25em}[0pt][0pt]{%
    \includegraphics[height=\baselineskip]{glyphs/joker.png}\!\!\!\!
  }
}
\fi
% List Extras and Jokers
\newcommand\jokeritem{\item[\jokeritemglyph]}


% Setting the attributes
\newcommand\agility[1]{\def\theagility{#1}}
\newcommand\smarts[1]{\def\thesmarts{#1}}
\newcommand\spirit[1]{\def\thespirit{#1}}
\newcommand\strength[1]{\def\thestrength{#1}}
\newcommand\vigor[1]{\def\thevigor{#1}}

\newcommand\settingattrname[1]{\def\thesettingattrname{#1}}
\newcommand\settingattr[1]{\def\thesettingattr{#1}}

\newcommand\pace[1]{\def\thepace{#1}}
\newcommand\parry[1]{\def\theparry{#1}}
\newcommand\toughness[1]{\def\thetoughness{#1}}

\newcommand\skills[1]{\def\theskills{#1}}
\newcommand\edges[1]{\def\theedges{#1}}
\newcommand\hindrances[1]{\def\thehindrances{#1}}
\newcommand\cyberware[1]{\def\thecyberware{#1}}
\newcommand\languages[1]{\def\thelanguages{#1}}

\newcommand\block[1]{%
  {\vskip .75em plus .25em minus .25em}%
  {\large\titlestyle #1}\par%
  {\vskip .25em plus .15em minus .15em}\relax%
}

\newtcolorbox{tcb@statblock}[1]{%
  enhanced jigsaw,
  breakable,
  arc=7pt,
  boxrule=0pt,
  left=0pt,
  right=0pt,
  colback=\tablebackground,
  coltitle=\tabletitlefg,
  colbacktitle=\tabletitlebg,
  fonttitle=\Large\titlestyle,
  title={#1},
  before skip=7pt plus 2pt minus 2pt,
  before upper={%
    \rowcolors{1}{\tablehighlight}{\tablehighlight}
    \def\arraystretch{1.2}
    \setlength\arrayrulewidth{.75pt}
    \setlength{\parindent}{0pt}
  }
}


%% ExpandWounds command
\def\expandwounds#1{\@expandwounds#1(\relax)}
\def\@expandwounds#1(#2){%
  \@@expandwounds{#1}%
  \ifx\relax#2\else(\@@expandwounds{#2})\expandafter\@expandwounds\fi}
\def\@@expandwounds#1{%
  \ifnum\numexpr0#1\relax > 0%
  \heart\expandafter\@@expandwounds\expandafter{\the\numexpr#1-1\relax}%
  \fi}

% Common environment code
\newif\if@joker \@jokerfalse

\newenvironment{@statblock}[3][0]{%
  % Default definitions
  \agility{--}
  \smarts{--}
  \spirit{--}
  \strength{--}
  \vigor{--}
  \pace{--}
  \parry{--}
  \toughness{--}
  \let\theskills\undefined
  \let\theedges\undefined
  \let\thehindrances\undefined
  \let\thecyberware\undefined
  \let\thelanguages\undefined
  \let\thesettingattr\undefined
  \begin{tcb@statblock}{\if@joker\jokerglyph\else\,\fi #2\hfill\expandwounds{#1}}
    #3 % Set the statistics
    \begin{itemize}[label={},leftmargin=.5pt]
    \item \textbf{\sffamily\pacename\colonspace:} {\thepace}.
      \ifdefined\thesettingattr
      \hfill\textbf{\sffamily\thesettingattrname\colonspace:} {\thesettingattr}.\,
      \fi
    \end{itemize}

    {\sffamily
      \attributesarray
    }

    \begin{itemize}[label={},leftmargin=.5pt]
      \ifdefined\theskills
    \item \textbf{\sffamily\skillsname\colonspace:} \theskills
      \fi
      \ifdefined\thehindrances
    \item \textbf{\sffamily\hindrancesname\colonspace:} \thehindrances
      \fi
      \ifdefined\theedges
    \item \textbf{\sffamily\edgesname\colonspace:} \theedges
      \fi
      \ifdefined\thecyberware
    \item \textbf{\sffamily\cyberwarename\colonspace:} \thecyberware
      \fi
      \ifdefined\thelanguages
    \item \textbf{\sffamily\languagesname\colonspace:} \thelanguages
      \fi
    \end{itemize}

    {\sffamily
      \begin{tabularx}{\textwidth}{CC}
        \hline
        \textbf{\parryname} & \textbf{\toughnessname} \\
        \theparry & \thetoughness \\
        \hline
      \end{tabularx}
    }
    \setlist{topsep=0pt}
  }{%
  \end{tcb@statblock}
  \let\theagility\undefined
  \let\thesmarts\undefined
  \let\thespirit\undefined
  \let\thestrength\undefined
  \let\thevigor\undefined
  \let\thepace\undefined
  \let\thesize\undefined
  \let\theparry\undefined
  \let\thetoughness\undefined
  \let\thepowerpoints\undefined
  \let\theskills\undefined
  \let\theedges\undefined
  \let\thehindrances\undefined
  \let\thecyberware\undefined
  \let\thelanguages\undefined
  \let\thesettingattr\undefined
}

% Extra statblock
\newenvironment{statblock}[3][0]
  {\vspace{.5em plus .25em minus .25em}%
    \@jokerfalse\begin{@statblock}[#1]{#2}{#3}}
  {\end{@statblock}\@jokerfalse}

%% Joker statblocks
\newenvironment{statblock*}[3][3]
  {\vspace{.5em plus .25em minus .25em}%
    \@jokertrue\begin{@statblock}[#1]{#2}{#3}}
  {\end{@statblock}\@jokerfalse}

%% Savage picture environment
\NewDocumentCommand\savagepicture{ s O{} m m }{%
  \IfBooleanTF#1%
  {% Starred command
    \begin{figure*}[t!]
      \centering
      \vspace{-1em}
      \begin{tikzpicture}
        \node[clip,inner sep=0,outer sep=0,rounded corners=0.25cm] {%
          \includegraphics[width=\textwidth,#2]{#4}
        };
        \node[inner sep=0,outer sep=0,opacity=0] {%
          \includegraphics[width=\textwidth,#2]{#4}
        };
      \end{tikzpicture}\def\tmp{#3}\ifx\tmp\@empty\else\vspace{-.65em}
      \parbox[t]{.98\textwidth}{%
        \raggedleft\tiny\bfseries\sffamily\textcolor{gray!85}{#3}
      }%
      \fi
    \end{figure*}
  }
  {% Non-starred command
    \begin{center}
      \begin{minipage}{\linewidth}
        \centering
      \begin{tikzpicture}
        \node[clip,inner sep=0,outer sep=0,rounded corners=0.25cm] {%
          \includegraphics[width=\linewidth,#2]{#4}
        };
        \node[inner sep=0,outer sep=0,opacity=0] {%
          \includegraphics[width=\linewidth,#2]{#4}
        };
      \end{tikzpicture}\def\tmp{#3}\ifx\tmp\@empty\else\vspace{-.65em}
      \parbox[t]{.97\linewidth}{%
        \raggedleft\tiny\bfseries\sffamily\textcolor{gray!85}{#3}%
      }%
      \fi
      \end{minipage}
    \end{center}
  }
}

%% Redefining commands and environments
\newtcolorbox{blockquote}{
  enhanced jigsaw,
  colback=\quotebg,
  before upper={\setlength\parindent{\savedparindent}},
  boxrule=0pt,
  boxsep=0pt,
  after skip=.5em plus .25em minus .25em,
  before skip=.5em plus .25em minus .25em,
  breakable
}
\renewenvironment{quote}{\begin{blockquote}\itshape}{\end{blockquote}}


%% Note environment

% Torn page effect
% https://tex.stackexchange.com/questions/86150/torn-page-effect

\pgfmathsetseed{1} % To have predictable results

% Define a background layer, in which the parchment shape is drawn
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

% This is the base for the fractal decoration. It takes a random point
% between the start and end, and raises it a random amount, thus
% transforming a segment into two, connected at that raised point This
% decoration can be applied again to each one of the resulting
% segments and so on, in a similar way of a Koch snowflake.
\pgfdeclaredecoration{irregular fractal line}{init}
{
  \state{init}[width=\pgfdecoratedinputsegmentremainingdistance]
  {
    \pgfpathlineto{%
      \pgfpoint{random * \pgfdecoratedinputsegmentremainingdistance}{%
        (random * \pgfdecorationsegmentamplitude - 0.02) *
         \pgfdecoratedinputsegmentremainingdistance}}
    \pgfpathlineto{\pgfpoint{\pgfdecoratedinputsegmentremainingdistance}{0pt}}
  }
}


% Define paper style and various commands
\def\notebgpict{pictures/note-background-0.jpg}
\tikzset{
  paper/.style={%
    draw=black!10,
    blur shadow,
    path picture={
      \node at (path picture bounding box.center) {
        \includegraphics[width=\linewidth]{\notebgpict}};}
  },
  irregular border/.style={%
    decoration={irregular fractal line, amplitude=0.2},
    decorate,
  },
  ragged border/.style={%
    decoration={random steps, segment length=8mm, amplitude=1.5mm},
    decorate,
  }
}

\newtcolorbox{tcbnote}[2][]{%
  enhanced jigsaw, blankest,
  before upper={\setlength\parindent{\savedparindent}},
  interior code={%
    \begin{pgfonlayer}{background}  % Draw the shape behind
      \fill[paper] % recursively decorate the bottom border
      decorate[irregular border]{%
        decorate{decorate{decorate{decorate[ragged border]{
                ($(interior.south east) - (0, random * 3mm)$) --
                ($(interior.south west) - (0, random * 3mm)$)
              }
            }
          }
        }
      }
      -- (interior.north west) -- (interior.north east) -- cycle;
    \end{pgfonlayer}
  },
  boxrule=0pt,
  left=8pt,
  right=8pt,
  top=5pt,
  bottom=5pt,
  coltitle=black,
  fonttitle=\titlestyle,
  attach boxed title to top center={yshift*=-\tcboxedtitleheight},
  title={#2},
  {#1}
}

\newtcolorbox{tcbnoteprint}[2][]{%
  enhanced jigsaw,
  before upper={\setlength\parindent{\savedparindent}},
  boxrule=1pt,
  rounded corners,
  left=8pt,
  right=8pt,
  top=5pt,
  bottom=5pt,
  coltitle=black,
  fonttitle=\titlestyle,
  attach boxed title to top center={yshift*=-\tcboxedtitleheight},
  title={#2},
  {#1}
}

%% Note environment (one column)
\newenvironment{note}[2][0]{%
  % Selecting background picture
  \ifnumcomp{#1}{=}{0}{\def\notebgpict{pictures/note-background-0.jpg}}{}%
  \ifnumcomp{#1}{=}{1}{\def\notebgpict{pictures/note-background-1.jpg}}{}%
  \ifnumcomp{#1}{=}{2}{\def\notebgpict{pictures/note-background-2.jpg}}{}%
  \ifnumcomp{#1}{=}{3}{\def\notebgpict{pictures/note-background-3.jpg}}{}%
  \ifnumcomp{#1}{=}{4}{\def\notebgpict{pictures/note-background-4.jpg}}{}%
  \ifnumcomp{#1}{=}{5}{\def\notebgpict{pictures/note-background-5.jpg}}{}%
  % tcolorbox environment
  \vspace{.75em plus .25em minus .25em}
  \if@noprint
    \begin{tcbnote}{#2}
  \else
    \begin{tcbnoteprint}{#2}
  \fi
}{%
  \if@noprint
    \end{tcbnote}
  \else
    \end{tcbnoteprint}
  \fi
  \vspace{1.25em plus .25em minus .25em}
  \def\notebgpict{pictures/note-background-0.jpg}
}

%% Note environment (full page)
\newenvironment{note*}[2][0]{%
  \begin{figure*}[htbp]
    \centering
    \begin{note}[#1]{#2}
  }{%
    \end{note}
  \end{figure*}
}

%% Savage section boxes
\newtcolorbox{tcbsection}[1][]{%
  enhanced jigsaw, blankest,
  interior code={%
    \begin{pgfonlayer}{background}  % Draw the shape behind
      \fill[paper] % recursively decorate the bottom border
      decorate[irregular border]{%
        decorate{decorate{decorate{decorate[ragged border]{
                ($(interior.south east) - (0, random * 2mm)$) --
                ($(interior.south west) - (0, random * 2mm)$)
              }
            }
          }
        }
      }
      -- (interior.north west) -- (interior.north east) -- cycle;
    \end{pgfonlayer}
  },
  boxrule=0pt,
  left=2pt,
  right=2pt,
  top=8pt,
  bottom=8pt,
  halign=center,
  valign=center,
  {#1}
}

\newtcolorbox{tcbsectionprint}[1][]{%
  enhanced jigsaw,
  boxrule=1pt,
  rounded corners,
  left=2pt,
  right=2pt,
  top=8pt,
  bottom=8pt,
  halign=center,
  valign=center,
  {#1}
}


%% Savage section environment (one column)
\newenvironment{savagesection}[1]{%
  \if@noprint
    \begin{tcbsection}{#1}
  \else
    \begin{tcbsectionprint}{#1}
  \fi
}{%
  \if@noprint
    \end{tcbsection}
  \else
    \end{tcbsectionprint}
  \fi
}

%% Miscellaneous additional commands
\DeclareFontFamily{U}{FdSymbolA}{}
\DeclareFontShape{U}{FdSymbolA}{m}{n}{<->FdSymbolA-Book}{}
\DeclareSymbolFont{extrasymbols}{U}{FdSymbolA}{m}{n}
\DeclareMathSymbol{\varheartsuit}{\mathord}{extrasymbols}{184}
\DeclareMathSymbol{\medblacksquare}{\mathord}{extrasymbols}{118}
\DeclareMathSymbol{\medblackbullet}{\mathord}{extrasymbols}{99}

\newcommand\heart{\ensuremath{\varheartsuit\xspace}}

%% Itemize bullets
\setitemize[0]{leftmargin=15pt,itemsep=0em}
\setenumerate[2]{label={\roman*.}}

\AfterEndPreamble{%
  \if@noprint
    \renewcommand{\labelitemi}%
      {\textcolor{\structure}{\ensuremath{\medblacksquare}\!}}
    \renewcommand{\labelitemii}%
      {\textcolor{\structure}{\ensuremath{\medblackbullet}\!}}
    \renewcommand{\labelitemiii}%
      {\textcolor{\structure}{\ensuremath{\blacktriangleright}\!}}
    \renewcommand{\labelitemiv}%
      {\textcolor{\structure}{\bfseries\textendash}}
  \else
    \renewcommand{\labelitemi}%
      {\textcolor{\structurefg}{\ensuremath{\medblacksquare}}}
    \renewcommand{\labelitemii}%
      {\textcolor{\structurefg}{\Large\textbullet\!}}
    \renewcommand{\labelitemiii}%
      {\textcolor{\structurefg}{\ensure{\blacktriangleright}\!}}
    \renewcommand{\labelitemiv}%
      {\textcolor{\structurefg}{\bfseries\textendash}}
  \fi

  \renewcommand{\theenumiii}{\alph{enumiii}\relax}
  \renewcommand{\theenumiv}{\Alph{enumiv}\relax}
}
