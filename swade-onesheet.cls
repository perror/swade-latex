%%
%% This file provide a LaTeX document class for onesheets written by
%% fans of the Savage Worlds RPG. This is an open-source work, so feel
%% free to share it and to modify it according to the license below.
%%
%% -------------------------------------------------------------------
%%
%% Copyright (C) 2021, Emmanuel Fleury <emmanuel.fleury@gmail.com>
%%
%% Permission to use, copy, modify, and/or distribute this software
%% for any purpose with or without fee is hereby granted.
%%
%% THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
%% WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
%% AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
%% CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
%% LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
%% NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
%% CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
%%

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{swade-onesheet}[2021/01/01 v0.4
SWADE onesheet document class]

\newif\if@noprint       \@noprinttrue
\newif\if@landscape     \@landscapefalse

\def\swade@fontsize{}
\def\swade@papersize{}

% Class options
\DeclareOption{a4paper}{\def\swade@papersize{a4paper}}
\DeclareOption{a5paper}{\def\swade@papersize{a5paper}}
\DeclareOption{letterpaper}{\def\swade@papersize{letterpaper}}

\DeclareOption{8pt}{\def\swade@fontsize{8pt}}
\DeclareOption{9pt}{\def\swade@fontsize{9pt}}
\DeclareOption{10pt}{\def\swade@fontsize{10pt}}
\DeclareOption{11pt}{\def\swade@fontsize{11pt}}
\DeclareOption{12pt}{\def\swade@fontsize{12pt}}

\DeclareOption{landscape}{\@landscapetrue}
\DeclareOption{print}{\@noprintfalse}

\ProcessOptions

\PassOptionsToClass{\swade@papersize}{extarticle}
\PassOptionsToClass{\swade@fontsize}{extarticle}

\if@landscape
  \PassOptionsToClass{landscape}{extarticle}
\fi

\LoadClass[twocolumn]{extarticle}

%%%%% Loading mandatory packages %%%%%

%% Loading common swade utilities
\input{swade-common.ltx}

%% Setting geometry
\RequirePackage[driver=pdftex,
                textwidth=.9\paperwidth,
                columnsep=16pt,
                top=.05\paperheight,
                bottom=.075\paperheight]{geometry}

%% Set pages style
\AtBeginDocument{%
  \pagestyle{empty}
}


%%%%% Specific to swade-onesheet class %%%%%
\AddToShipoutPictureBG{%
  \if@noprint
    \begin{ocg}{Background}{bckgrd}{1}
      \includegraphics[width=\paperwidth,%
                       height=\paperheight]{pictures/onesheet-background.jpg}
    \end{ocg}
  \fi
  \ifnumcomp{\thepage}{>}{1}{
    \tikz[remember picture,overlay]{%
      \node at ([yshift=20pt]current page.south)
      {\Large\bfseries\sffamily\thepage};
    }
  }{}
}

%%% Redfining sections titles
\titleformat{\section}
            {\normalfont\Large\bfseries\sffamily}
            {}
            {0em}
            {\MakeUppercase{\textls[-50]{#1}}}
            [{\titlerule[1pt]}]
\titlespacing*{\section}
{0pt}{2.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\titleformat{\subsection}
            {\normalfont\large\bfseries\sffamily}
            {}
            {0em}
            {\MakeUppercase{\textls[-50]{#1}}}
\titlespacing*{\subsection}
{0pt}{1.75ex plus 1ex minus .2ex}{1ex plus .2ex}

\titleformat{\subsubsection}
            {\normalfont\normalsize\bfseries\sffamily}
            {}
            {0em}
            {\MakeUppercase{\textls[-50]{#1}}}
\titlespacing*{\subsection}
{0pt}{1.25ex plus 1ex minus .2ex}{.5ex plus .2ex}

\titleformat{\paragraph}[runin]
            {\normalfont\normalsize\bfseries\sffamily\scshape}
            {}
            {0em}
            {#1{\normalfont \colonspace:}}
\titlespacing*{\paragraph}{0pt}{.75ex plus .5ex minus .25ex}{.5em}

%% Table of contents
\setcounter{tocdepth}{1}

\titlecontents{section}[0em]
              {\sffamily\scshape}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}

\titlecontents{subsection}[1em]
              {\sffamily}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}

\titlecontents{subsubsection}[3em]
              {\sffamily}
              {}
              {}
              {\titlerule*[6pt]{.}\contentspage}


%% Redefining various command because of the titlepage option
\renewenvironment{abstract}{%
  \if@twocolumn
    \section*{\abstractname}%
  \else\small
    \begin{center}%
      {\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
    \end{center}%
  \quotation
  \fi
}
{\if@twocolumn\else\endquotation\fi}

\newlength\topoffset
\setlength\topoffset{\voffset}
\addtolength\topoffset{\topmargin}
\addtolength\topoffset{\headheight}
\addtolength\topoffset{\headsep}
\addtolength\topoffset{1in}
\addtolength\topoffset{2pt}


\if@noprint
  \newtcolorbox{onesheettitle}{%
    enhanced jigsaw, blankest,
    before upper={\centering},
    watermark graphics={pictures/onesheet-top-banner.png},
    watermark stretch=1.00,
    opacityback=0,
    boxrule=0pt,
    left=25pt,
    right=25pt,
    top=25pt,
    bottom=25pt,
    width=1.05\paperwidth,
    center,
    before={\vskip -\topoffset},
  }
\else
  \newtcolorbox{onesheettitle}{%
    enhanced jigsaw, blankest,
    before upper={\centering},
    opacityback=0,
    boxrule=0pt,
    left=25pt,
    right=25pt,
    top=25pt,
    bottom=25pt,
    width=1.05\paperwidth,
    center,
    before={\vskip -\topoffset},
  }
\fi


%%%%% Maketitle command %%%%%
\renewcommand\maketitle{\par
  \begingroup
  \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
  \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
  \long\def\@makefntext##1{\parindent 1em\noindent
    \hb@xt@1.8em{%
      \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
  \if@twocolumn
    \ifnum \col@number=\@ne
      \@maketitle
    \else
      \twocolumn[\@maketitle]%
    \fi
  \else
    \newpage
    \global\@topnum\z@   % Prevents figures from going at top of page.
    \@maketitle
  \fi
  \@thanks
  \enlargethispage{-.1\paperheight}%
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \newpage
  \null
  \begin{onesheettitle}%
    \let \footnote \thanks
    \begin{minipage}{.685\paperwidth}
      \centering
      \HUGE\titlestyle%
      \@title
    \end{minipage}
    \ifdefined\thesubtitle
    \vskip 1.25em
    \begin{minipage}{.75\paperwidth}
      \centering
      \large\sffamily\bfseries%
      \thesubtitle
    \end{minipage}
    \fi
  \end{onesheettitle}%
  \par%
  \vskip 1.5em%
  \begin{tikzpicture}[remember picture,overlay]
    \if@noprint
      \node[anchor=south,inner sep=0,outer sep=0] at (current page.south)
        {
        \includegraphics[width=\paperwidth,height=.145\paperheight]
          {pictures/onesheet-bottom-banner.png}
      };
    \fi
  \end{tikzpicture}
  \thecopyrightnotice
  \afterpage{\enlargethispage{-.1\paperheight}}%
}

\endinput
