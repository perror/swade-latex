%%
%% This file provide a LaTeX document class for articles written by
%% fans of the Savage Worlds RPG. This is an open-source work, so feel
%% free to share it and to modify it according to the license below.
%%
%% -------------------------------------------------------------------
%%
%% Copyright (C) 2021, Emmanuel Fleury <emmanuel.fleury@gmail.com>
%%
%% Permission to use, copy, modify, and/or distribute this software
%% for any purpose with or without fee is hereby granted.
%%
%% THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
%% WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
%% WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
%% AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
%% CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
%% LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
%% NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
%% CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
%%

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{swade-cards}[2021/01/01 v0.4
SWADE adventure cards class]

\newif\if@oversized  \@oversizedfalse

\def\swade@papersize{a4paper}

% Class options
\DeclareOption{a4paper}{\def\swade@papersize{a4paper}}
\DeclareOption{letterpaper}{\def\swade@papersize{letterpaper}}

\DeclareOption{oversized}{\@oversizedtrue}

\ProcessOptions

\PassOptionsToClass{\swade@papersize}{article}


%% Adjusting for oversised cards
\if@oversized
  \PassOptionsToClass{12pt}{article}
\else
  \PassOptionsToClass{10pt}{article}
\fi

\LoadClass{article}

%% Miscellaneous packages
\RequirePackage{ifthen}

%% Font packages
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{palatino}
\RequirePackage{amssymb}

%% Graphics
\RequirePackage{afterpage}
\RequirePackage{eso-pic}
\RequirePackage{etoolbox}
\RequirePackage{graphicx}
\RequirePackage{pgfkeys}
\RequirePackage{pgfmath}
\RequirePackage{tikz} %% Tikz is last to avoid conflicts
\usetikzlibrary{calc,patterns,positioning,shadows}


%% Babel specific translations
\newcommand\cardbacktitle{Adventure Cards}

\AtBeginDocument{%
  \@ifpackageloaded{babel}{%
    %%% French
    \addto{\extrasfrench}{%
      \renewcommand\cardbacktitle{Cartes d'Aventure}
    }
  }{}
}


%% Removing all unnecessary decorations
\null\pagestyle{empty}


%% Get necessary card sizes and derived values
\newlength\cardwidth
\newlength\cardheight
\if@oversized
  \setlength\cardwidth{88mm}
  \setlength\cardheight{125mm}
\else
  \setlength\cardwidth{62mm}
  \setlength\cardheight{88mm}
\fi

\newlength\papermargin
\setlength\papermargin{10pt}
\newlength\cardmargin
\setlength\cardmargin{5pt}

%% \paperwidth = 2\papermargin + (\cardwidth + 2\cardmargin) * \imaxcards
\pgfmathsetmacro\imaxcards{%
  int((\paperwidth - 2\papermargin) / (\cardwidth + 2\cardmargin)) - 1
}

%% \paperheight = 2\papermargin + (\cardheight + 2\cardmargin) * \jmaxcards
\pgfmathsetmacro\jmaxcards{%
  int((\paperheight - 2\papermargin) / (\cardheight + 2\cardmargin)) - 1
}

\newlength\ioffset
\setlength\ioffset{\cardwidth}
\addtolength\ioffset{2\cardmargin}
\newlength\joffset
\setlength\joffset{\cardheight}
\addtolength\joffset{2\cardmargin}

\pgfmathsetmacro\cardsperpage{int((\imaxcards + 1) * (\jmaxcards + 1))}

%% Set page backgrounds
\AddToShipoutPictureBG{%
  \ifthenelse{\isodd{\value{page}}}{%
    \begin{tikzpicture}[remember picture,overlay]
      \tikzset{shift={(current page.north west)},
        xshift=\papermargin+\cardmargin+.5\cardwidth,
        yshift=-\papermargin-\cardmargin-.5\cardheight}
      \foreach \i in {0, ..., \imaxcards}
        \foreach \j in {0, ..., \jmaxcards} {
          \node at (\ioffset*\i, -\joffset*\j) {
            \includegraphics[width=\cardwidth,height=\cardheight]
              {pictures/swade-adventure_card_front_background.jpg}};
        }
      \end{tikzpicture}
  }{%
    \begin{tikzpicture}[remember picture,overlay]
      \tikzset{shift={(current page.north east)},
        xshift=-\papermargin-\cardmargin-.5\cardwidth,
        yshift=-\papermargin-\cardmargin-.5\cardheight}
      \foreach \i in {0, ..., \imaxcards}
        \foreach \j in {0, ..., \jmaxcards} {
          \node at (-\ioffset*\i, -\joffset*\j) {%
            \includegraphics[width=\cardwidth,height=\cardheight]
              {pictures/swade-adventure_card_back.jpg}};
          \node at (-\ioffset*\i, -\joffset*\j-.34\cardheight)
            {\Large\bfseries\scshape\sffamily \textcolor{white}{\cardbacktitle}};
        }
    \end{tikzpicture}
  }
}

% New variables to track down cards' positions
\newcounter{cardid}
\newlength\icoord
\newlength\jcoord
\newlength\pagecardid

\newcommand\card[3]{%
  % Calculate the current coordinates
  \pgfmathparse{mod(\value{cardid},\imaxcards + 1)}
  \pgfmathtruncatemacro{\icoord}{\pgfmathresult}
  \pgfmathparse{\value{cardid} / (\imaxcards + 1)}
  \pgfmathtruncatemacro{\jcoord}{\pgfmathresult}

  % Calculates the current page
  \pgfmathparse{int(mod(\value{cardid},\cardsperpage))}
  \pgfmathtruncatemacro{\pagecardid}{\pgfmathresult}

  \ifnumcomp{\pagecardid}{=}{0}{\afterpage{\mbox{}}}{}

  % Display the pictures
  \begin{tikzpicture}[remember picture,overlay]
    \tikzset{shift={(current page.north west)},
      xshift=\papermargin+\cardmargin+.5\cardwidth,
      yshift=-\papermargin-\cardmargin-.5\cardheight}
    \node[anchor=south] at (\ioffset*\icoord,-\joffset*\jcoord-4pt) {%
      \includegraphics[width=\cardwidth,height=.5\cardheight]{#1}
    };
    \node[anchor=south] at (\ioffset*\icoord,-\joffset*\jcoord-.5\cardheight-4pt) {%
      \includegraphics[width=\cardwidth,height=\cardheight]
        {pictures/swade-adventure_card_front_foreground.png}
    };
    \node[anchor=north] at (\ioffset*\icoord,-\joffset*\jcoord-.25em) {%
      \begin{minipage}{.925\cardwidth}
        \centering
        \sffamily
        \textbf{\LARGE #2}\\[.5em]
        #3
      \end{minipage}
    };
  \end{tikzpicture}

  \ifnumcomp{\pagecardid}{=}{\cardsperpage - 1}{%
    \clearpage\newpage
    \setcounter{cardid}{0}
  }{\stepcounter{cardid}}
}

\endinput
